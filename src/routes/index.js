import Dashboard from "./../components/admin/Dashboard";
import User from "./../components/admin/User";
import Default from "../screens/Default";

const routes = [{
        path: "/dashboard",
        component: Dashboard
    },
    {
        path: "/user",
        component: User
    },
    {
        path: "/default",
        component: Default
    }
];

export default routes;