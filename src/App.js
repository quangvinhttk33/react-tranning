import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from "react-router-dom";
import AdminLayout from "./screens/AdminLayout";
import Login from "./screens/Login";
import routes from "./routes";

import NoMatch from "./components/admin/NoMatch";

const PrivateRoute = ({ Component, ...rest }) => (
<Route
{...rest}
render={props =>
localStorage.getItem("user") ? (
<AdminLayout>
<Component {...props} /> <
/AdminLayout>
): (
<Redirect
to={{ pathname: "/login", state: { from: props.location } }}
/>
)
}
/>
);

export default class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    {routes &&
                        routes.map(route => (
                            <PrivateRoute
                                key={route.path}
                                path={route.path}
                                Component={route.component}
                            />
                        ))}
                    <Route exact path="/" component={Login} />
                    <Route exact path="/login" component={Login} />
                    <Route component={NoMatch} />
                </Switch>
            </Router>
        );
    }
}