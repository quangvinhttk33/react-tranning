import React from "react";
import Header from "./../components/admin/Header";
import MainSidebar from "./../components/admin/MainSidebar";
import Footer from "./../components/admin/Footer";

export default class AdminLayout extends React.Component {
	render() {
		return (
			<div className="AdminLayout">
				<div className="wrapper">
					<Header />
					<MainSidebar />
					<div
						className="content-wrapper"
						style={{ minHeight: "946px" }}
					>
						{this.props.children}
					</div>
					<Footer />
					<div className="control-sidebar-bg" />
				</div>
			</div>
		);
	}
}
