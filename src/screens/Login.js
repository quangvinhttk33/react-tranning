import React from "react";

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            isEmail: false,
            errorUnauthorized: "",
        };
    }

    handleLoginAdmin = event => {
        const { email, password } = this.state;
        if (this.validateEmail(email)) {
            this.setState({
                isEmail: false
            });
            const requestOptions = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ email, password })
            };
            return fetch("http://10.9.11.244:3000/users/authenticate", requestOptions)
                .then(this.handleResponse)
                .then(user => {
                    // login successful if there's a user in the response
                    if (user) {
                        // store user details and basic auth credentials in local storage
                        // to keep user logged in between page refreshes
                        user.authdata = window.btoa(email + ":" + password);
                        window.localStorage.setItem(
                            "user",
                            JSON.stringify(user)
                        );
                        this.props.history.push("/dashboard");
                    }
                    return user;
                });
        }
        this.setState({
            isEmail: true
        });
        return;
    };

    logout = () => {
        // remove user from local storage to log user out
        localStorage.removeItem("user");
    };

    handleResponse = response => {
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                this.logout();
            }
            const error = (response && response.message) || response.statusText;
            this.setState({
                errorUnauthorized: "Sai tên đăng nhập hoặc mật khẩu"
            });
            return Promise.reject(error);
        } else {
            return response.text().then(text => {
                const data = text && JSON.parse(text);
                return data;
            });
        }
    };
    validateEmail = email => {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };

    render() {
        const error = <p style={{ color: "red" }}>Địa chỉ Email không hợp lệ</p>;
        return (
            <div className="login-box">
				<div className="login-logo">
					<a href="../../index2.html">
						<strong>Admin</strong>LOGIN
					</a>
				</div>
				{/* /.login-logo */}
				<div className="login-box-body">
					<p className="login-box-msg">
						Sign in to start your session
					</p>
                    <div>
                    <p>dữ liệu test</p>
                    <p><strong>Email: </strong><span>test@gmail.com</span></p>
                    <p><strong>Password: </strong><span>123456</span></p>
                    </div>
					<div className="form">
						<div className="form-group has-feedback">
							<input
								type="text"
								name="email"
								className="form-control"
								placeholder="Email"
								onChange={event =>
									this.setState({
										email: event.target.value
									})
								}
							/>
							<span className="glyphicon glyphicon-envelope form-control-feedback" />
							{this.state.isEmail && error}
						</div>
						<div className="form-group has-feedback">
							<input
								type="password"
								className="form-control"
								placeholder="Password"
								onChange={event =>
									this.setState({
										password: event.target.value
									})
								}
							/>
							<span className="glyphicon glyphicon-lock form-control-feedback" />
						</div>
						<div>
							<p style={{ color: "red" }}>
								{this.state.errorUnauthorized}
							</p>
						</div>
						<div className="row">
							{/* /.col */}
							<div className="col-xs-6 col-xs-offset-3">
								<button
									type="submit"
									className="btn btn-primary btn-block btn-flat"
									onClick={event =>
										this.handleLoginAdmin(event)
									}
								>
									Login
								</button>
							</div>
							{/* /.col */}
						</div>
					</div>
				</div>
				{/* /.login-box-body */}
			</div>
        );
    }
}