import React from "react";
import { Link } from "react-router-dom";

export default class NoMatch extends React.Component {
  render() {
    return (
      <div className="login-box">
        <div className="login-logo">
          <p href="../../index2.html">
            <strong>404 </strong>PAGE NOT FOUND
          </p>
          <Link to="/dashboard">Click to Dashboard</Link>
        </div>
      </div>
    );
  }
}
