import React from "react";
import { NavLink } from "react-router-dom";

export default class MainSidebar extends React.Component {
  render() {
    return (
      <aside className="main-sidebar">
        {/* sidebar: style can be found in sidebar.less */}
        <section className="sidebar">
          {/* sidebar menu: : style can be found in sidebar.less */}
          <ul className="sidebar-menu">
            <li className="header">MAIN NAVIGATION</li>
            <li className="treeview">
              <NavLink to="/dashboard" activeClassName="active">
                <i className="fa fa-edit" /> <span>Dashboard</span>
                <span className="pull-right-container"></span>
              </NavLink>
            </li>
            <li className="treeview">
              <NavLink to="/user" activeClassName="active">
                <i className="fa fa-table" /> <span>User</span>
                <span className="pull-right-container"></span>
              </NavLink>
            </li>
          </ul>
        </section>
        {/* /.sidebar */}
      </aside>
    );
  }
}
