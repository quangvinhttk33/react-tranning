import React, { useState } from "react";

// export default class User extends React.Component {
//   render() {
//     return (
//       <section className="content-header">
//         <h1>
//           User
//           <small>Control panel</small>
//         </h1>
//       </section>
//     );
//   }
// }
const listTaskDemo = [
  {
    idTask: '01',
    name: "task 01",
    status: 0
  },
  {
    idTask: '02',
    name: "task 02",
    status: 1
  },
  {
    idTask: '03',
    name: "task 03",
    status: 0
  }
]
function addTask(e) {
  alert(e);
}
function deteteTask(e) {
  alert('xóa phần tử ' + e);
}
function User() {
  const [taskName, setTaskName] = useState("");
  return (
    <section className="content-header">
      <h1>
        User
          <small>Control panel</small>
      </h1>
      <div>
        <div id="myDIV" className="header">
          <h2 style={{ margin: '5px' }}>My To Do List</h2>
          <input type="text" id="myInput" placeholder="Tên công việc..." onChange={(e) => setTaskName(e.target.value)} />
          <span onClick={() => addTask(taskName)} className="addBtn">Add</span>
        </div>
        <ul id="myUL">
          {listTaskDemo.map((item) => <li key={item.idTask}>{item.name}<span className="close" onClick={() => deteteTask(item.idTask)}>×</span></li>)}
        </ul>
      </div>
    </section>
  );
}
export default User;
