import React from "react";

export default class Dashboard extends React.Component {
  render() {
    return (
      <section className="content-header">
        <h1>
          Dashboard
          <small>Control panel</small>
        </h1>
      </section>
    );
  }
}
