import React from "react";
import { Link } from "react-router-dom";

export default class Header extends React.Component {
  logout = () => {
    // remove user from local storage to log user out
    localStorage.removeItem("user");
  };
  sidebarToggle = () => {
    let $body = document.querySelector("body");
    $body.classList.toggle("sidebar-collapse");
  };
  render() {
    return (
      <header className="main-header">
        {/* Logo */}
        <Link to="#" className="logo">
          {/* mini logo for sidebar mini 50x50 pixels */}
          <span className="logo-mini">
            <b>A</b>LT
          </span>
          {/* logo for regular state and mobile devices */}
          <span className="logo-lg">
            <b>Admin</b>LTE
          </span>
        </Link>
        {/* Header Navbar: style can be found in header.less */}
        <nav className="navbar navbar-static-top">
          {/* Sidebar toggle button*/}
          <Link
            to="#"
            className="sidebar-toggle"
            data-toggle="push-menu"
            role="button"
            onClick={this.sidebarToggle}
          >
            <span className="sr-only">Toggle navigation</span>
          </Link>
          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              <li className="user user-menu">
                <Link to="#" className="btn btn-flat" onClick={this.logout}>
                  Sign out
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}
